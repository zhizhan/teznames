This document contains the list of RESTful API services.  

## General Info

!!! summary "GET /teznames/v1/isRegistered"

    Check the given name is registered

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    registered :: boolean
    ```

!!! summary "GET /teznames/v1/isPremium"

    Check the given name is a premium name

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    registered :: boolean
    ```

!!! summary "GET /teznames/v1/isRestricted"

    Check the given name is a restricted name

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    registered :: boolean
    ```

!!! summary "GET /teznames/v1/restricted\_name\_root\_hash"

    Get the hash value from the root of the merkle tree of restricted names

    **Response (JSON)**

    200 success

    ```
    roothash :: string
    ```

!!! summary "GET /teznames/v1/premium\_name\_root\_hash"

    Get the hash value from the root of the merkle tree of premium names

    **Response (JSON)**

    200 success

    ```
    roothash :: string
    ```

!!! summary "GET /teznames/v1/registration\_fee"

    Query the fee of registering the given type of name

    **Query Parameter**

    ```
    nametype :: string -- "reqular" | "premium" | "restricted"
    ```

    **Response (JSON)**

    200 success

    ```
    fee :: integer
    ```


## Name-based Info

!!! summary "GET /teznames/v1/isExpired"

    Check the given name is expired

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    expired :: boolean
    ```

!!! summary "GET /teznames/v1/type"

    Query the type of the given name

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    expired :: string -- "regular" | "premium" | "restricted"
    ```

!!! summary "GET /teznames/v1/name/state"

    Query the state of the given name. A tezname can be:

    1. *registrable* - can be registered by any one
    2. *auctionable* - be registered recently and can be asked to start an auction procress
    3. *biddable* - in auction procress, any one can bid
    4. *registered* - owned by some one

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    state :: string -- "registrable" | auctionable" | "biddable | "registered"
    end_time :: timestamp -- [optional] only for "auctionable" or "biddable"

    ```

!!! summary "GET /teznames/v1/name/owner"

    Query the address of the owner of the given name

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    address :: string
    ```


!!! summary "GET /teznames/v1/name/last\_modification"

    Query the time and type of the last modification of the given name

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    time :: timestamp
    msg  :: string
    ```


!!! summary "GET /teznames/v1/name/timestamps"

    Query the all time-info of the given name

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    registered_time :: timestamp
    expired_time :: timestamp
    last_modification_date :: timestamp
    ```

!!! summary "GET /teznames/v1/name/timestamp/register"

    Query when is the registration time of the given name

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    time :: timestamp
    ```

!!! summary "GET /teznames/v1/name/timestamp/expire"

    Query when is the expiration time of the given name

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    time :: timestamp
    ```

!!! summary "GET /teznames/v1/name/timestamp/last\_modification"

    Query when is the last modification time of the given name

    **Query Parameter**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    time :: timestamp
    ```


## Name registration

!!! summary "POST /teznames/v1/register/regular"

    Request to register a regular name

    **Parameter (JSON)**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    tezname_address :: string
    ```

!!! summary "POST /teznames/v1/register/premium"

    Request to register a premium name

    **Parameter (JSON)**

    ```
    plaintext_name :: string
    merkle_proof :: Array [ string ]
    ```

    **Response (JSON)**

    200 success

    ```
    tezname_address :: string
    ```

!!! summary "POST /teznames/v1/register/restricted"

    Request to register a restricted name

    **Parameter (JSON)**

    ```
    plaintext_name :: string
    merkle_proof :: Array [ string ]
    document :: string -- check sum of legal document
    ```

    **Response (JSON)**

    200 success

    ```
    tezname_address :: string
    ```

!!! summary "POST /teznames/v1/register/subname"

    Request to register a subname on the given parent name

    **Parameter (JSON)**

    ```
    root_tezname :: string -- e.g. "john" for "john.tez"
    subname :: string -- e.g. "store" for "store.john.tez"
    ```

    **Response (JSON)**

    200 success

    ```
    tezname_address :: string
    ```

!!! summary "POST /teznames/v1/renew"

    Request to renew the ownership of the given name

    **Parameter (JSON)**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    msg :: string
    ```

!!! summary "POST /teznames/v1/requestAuction"

    Ask the targeting tezname to start an auction procress.

    **Parameter (JSON)**

    ```
    plaintext_name :: string
    ```

    **Response (JSON)**

    200 success

    ```
    msg :: string
    ```


!!! summary "POST /teznames/v1/bid/regular"

    Place the price for bidding the targeting name

    **Parameter (JSON)**

    ```
    plaintext_name :: string
    price :: integer
    ```

    **Response (JSON)**

    200 success

    ```
    tezname_address :: string
    ```

!!! summary "POST /teznames/v1/bid/premium"

    Place the price for bidding the targeting premium name

    **Parameter (JSON)**

    ```
    plaintext_name :: string
    merkle_proof :: Array [ string ]
    price :: integer
    ```

    **Response (JSON)**

    200 success

    ```
    tezname_address :: string
    ```

!!! summary "POST /teznames/v1/bid/restricted"

    Place the price for bidding the targeting restricted name

    **Parameter (JSON)**

    ```
    plaintext_name :: string
    merkle_proof :: Array [ string ]
    document :: string -- check sum of legal document
    price :: integer
    ```

    **Response (JSON)**

    200 success

    ```
    tezname_address :: string
    ```
