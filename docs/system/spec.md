!!! info "9chsTNS-0.0.1"

## cTezName

### contract storage

!!! done "Storage specification"

    ```
    type tQuestion = bytes
    type tAnswer   = signature
    type tIdentity = key * tQuestion
    type tSignal   = key * tAnswer * tQuestion
    type tResource = string
    type tDate     = timestamp
    type tModification = string
    type tDNS_Name = string
    type tDNS_IP   = string
    type tTzNameStorage =
       { sProtocol    : string
       ; sTzName      : tResource
       ; sAvailable   : bool
       ; sOwner       : tIdentity
       ; sAdmin       : tIdentity
       ; sDNS         : (tDNS_Name, tDNS_IP) map
       ; sAppliedDate      : tDate
       ; sExpireDate       : tDate
       ; sLastModification : tDate * tModification
       ; sRoot             : address (* for demo *)
       }
    type storage = tTzNameStorage
    ```

### contract entry

The _cTezName_ contains three major entries:

```
register :: (tSignal * tIdentity) -> storage -> (list operation * storage)
renew :: tSignal -> storage -> (list operation * storage)
free :: tSignal -> storage -> (list operation * storage)
```

All three `tSignal` typed parameters are for [signature-based identity](https://teznames.readthedocs.io/en/latest/system/overview/#tezname) verification. The `register` and `free` must be called by _admin_; yet, the `renew` can be called only by _owner_.

The second parameter of `register`, the one with type `tIdentity`, is a [signature-based identity](https://teznames.readthedocs.io/en/latest/system/overview/#tezname) which represents the new _owner_ (aka _registrant_).

!!! warning "The extra and demo-only entries"

    ```
    updateRoot :: address -> storage -> (list operation * storage)
    updateAdmin :: tIdentity -> storage -> (list operation * storage)
    ```

    The parameter of _updateRoot_ is the account address of new root; and, the parameter of _updateAdmin_ is the [signature-based identity](https://teznames.readthedocs.io/en/latest/system/overview/#tezname) of new admin

## cTNRecords

### contract storage

!!! done "Storage specification"

    ```
    type storage =
       { sProtocol : string
       ; sAdmin    : address
       ; sRecord   : (string, address) map
       }
    ```

### contract entry

There is only one entry.

!!! warning

    Currently the cTNRecords is just for showing the list of registered name, so it does no protocol or sender checking.

```
update (string * address) -> storage -> (list operation * storage)
```

The parameters stand for _plain name_ and _its contact address_.


!!! warning "The extra and demo-only entries"

    ```
    updateAdmin address -> storage -> (list operation * storage)
    ```

    The only parameter is for _admin_ to step-down and assign a _successor_.
