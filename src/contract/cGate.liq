(* ## outer type ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

type tMerkleProof = ((bytes, bytes) variant) list

type tTeznameInfo
  = RegularName
  | PremiumName    of tMerkleProof
  | RestrictedName of tMerkleProof * bytes

type tTeznameType
  = Regular | Premium | Restricted of bytes

(* ## inner type ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

type tAuctions
  = (bytes, (tez * address * timestamp)) map

type tCommission =
  { regP : tez
  ; preP : tez
  ; resP : tez
  }

type tGateStorage =
  { sAdmin      : address
  ; scNameTypes : address
  ; scRootname  : address
  ; sCommission : tCommission
  ; sAuctions   : tAuctions
  }

(* ## contract definition ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

contract type TeznameSpec = sig
  type storage
  val%entry resetOwnership : (address * address * tTeznameType) -> _
  val%entry updateSubnameRecord : (bytes * address) -> _
  val%entry update : ((address, address) variant) -> _
  val%entry renew : unit -> _
end

contract type NameTypesSpec = sig
  type storage
  val%entry nameCheck : (bytes * tTeznameInfo) -> _
  val%entry update : (bytes * bytes) -> _
end

(* ## local function ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

let[@inline] noop = ([] : operation list)

let[@inline] into2type info =
  match info with
    | RegularName -> Regular
    | PremiumName _ -> Premium
    | RestrictedName (_ , b) -> Restricted b

let[@inline] castNameTypes msg addr : NameTypesSpec.instance =
  begin match (Contract.at addr : NameTypesSpec.instance option) with
    | None -> Current.failwith msg;
    | Some inst -> inst
  end

(* ## contract definition ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

type storage = tGateStorage

(* role : user *)
let%entry register
  ( _hname , _TeznameInfo
  : bytes  * tTeznameInfo ) s =
  (* ### check existence of auction of target name ### *)
  if (Map.mem _hname s.sAuctions)
  then Current.failwith "[ERROR]register/locked";
  (* ### check commission ### *)
  if (Current.amount () < s.sCommission.regP)
  then Current.failwith "[ERROR]register/regularP";
  (* ### check the requested type of name ### *)
  let cNameTypes  = castNameTypes "[ERROR]register/castNameTypes" s.scNameTypes in
  let opNameCheck = cNameTypes.nameCheck (_hname, _TeznameInfo) ~amount:0tz in
  (* ### new auction record ### *)
  let registrant = Current.source () in
  let now = Current.time () in
  let amo = Current.amount () in
  let s = s.sAuctions <-
    Map.update _hname (Some (amo, registrant, now)) s.sAuctions in
  [ opNameCheck ] , s

(* role : admin *)
let%entry resolve
  ( _hname : bytes ) s =
  (* ### caller identity check ### *)
  if (Current.source () <> s.sAdmin)
  then Current.failwith "[ERROR]resolve/admin";
  (* ### update auction list ### *)
  let s = s.sAuctions <- Map.remove _hname s.sAuctions in
  noop , s

(* role : admin *)
let%entry setup
  ( _scNameTypes , _scRootname , _sCommission
  : address      * address     * tCommission ) s =
  (* ### caller identity check ### *)
  if (Current.source () <> s.sAdmin)
  then Current.failwith "[ERROR]accept/admin";
  let s = s.scNameTypes <- _scNameTypes in
  let s = s.scRootname  <- _scRootname in
  let s = s.sCommission <- _sCommission in
  noop , s
