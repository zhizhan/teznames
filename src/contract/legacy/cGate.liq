(* ## type ## *)
(* ↓↓↓↓↓↓↓↓↓↓ *)

type tHashedName = bytes

type tDestType
  = Wallet | IP | URL

type tResource = bytes
type tModification = string
type tMerkleProof = ((bytes, bytes) variant) list
type tRecords = (tHashedName, (address * tDestType) ) map

type tTeznameType
  = Regular | Premium | Restricted of bytes

type tTeznameInfo
  = RegularName
  | PremiumName    of tMerkleProof
  | RestrictedName of tMerkleProof * bytes

type tProtocol = string

type tRootHashValue
  = None
  | ForPremium of bytes
  | ForRestricted of bytes

type tMerkleRoot = bytes

type tFees =
  { sRgNameFee : tez
  ; sPrNameFee : tez
  ; sRtNameFee : tez
  }

type tIdx
  = Reg
  | Rew
  | Nac
  | RtN
  | Auc

type tGateStorage =
  { scRegistrar  : address
  ; scRenew      : address
  ; scNameTypes  : address
  ; scAuction    : address
  ; sManager     : key_hash
  ; sAdmin       : address
  ; sRootName    : address
  }

type storage = tGateStorage

(* ## contract ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

contract type TeznameSpec = sig
  type storage
  val%entry hasChild : bytes -> _
  val%entry noChild : bytes -> _
  val%entry hasParent : address -> _
  val%entry levelCheck : tTeznameType -> _
  val%entry oneYr : unit -> _
  val%entry pay : tTeznameType -> _
  val%entry updateSubnameRecord : (bytes  * address * tDestType) -> _
  val%entry updateOwnership : (string * tDestType * tTeznameType * tFees) -> _
  val%entry renew : unit -> _
  val%entry withdraw : UnitContract.instance -> _
end

contract type RegistrarSpec = sig
  type storage
  val%entry preregister : (address * address * bytes  * tTeznameType) -> _
  val%entry register : (address * address * bytes * string * tDestType * tTeznameType * tFees) -> _
end

contract type NameTypesSpec = sig
  type storage
  val%entry nameCheck : (bytes  * tTeznameInfo) -> _
end

contract type RenewSpec = sig
  type storage
  val%entry renew : (address * address * tTeznameType) -> _
end

(* ## function ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

let[@inline] info2type (i : tTeznameInfo) : tTeznameType =
  match i with
  | RegularName -> Regular
  | (PremiumName _) -> Premium
  | (RestrictedName (_,chsum)) -> Restricted chsum

let[@inline] castRegistrar s msg : RegistrarSpec.instance =
  begin match (Contract.at s.scRegistrar : RegistrarSpec.instance option) with
    | None -> Current.failwith msg;
    | Some c -> c
  end

let[@inline] castNameTypes s msg : NameTypesSpec.instance =
  begin match (Contract.at s.scNameTypes : NameTypesSpec.instance option) with
    | None -> Current.failwith msg;
    | Some c -> c
  end

let[@inline] castRenew s msg : RenewSpec.instance =
  begin match (Contract.at s.scRenew : RenewSpec.instance option) with
    | None -> Current.failwith msg;
    | Some c -> c
  end

(* ## contract entry ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

let%entry preregister
  ( _parentAddr , _addr , _hname  , _TeznameType
  : address     * address * bytes * tTeznameType ) s =
  if (Current.sender () <> s.sAdmin) then Current.failwith "[ERROR]preregister/0";
  let cRegistrar = castRegistrar s "[ERROR]preregister/1" in
  let param = (_parentAddr , _addr , _hname , _TeznameType) in
  let opPreRegister = cRegistrar.preregister param ~amount:0tz in
  ([opPreRegister] , s)

let%entry register
  ( _parentAddr , _addr , _hname , _Dest  , _DestType , _TeznameInfo , _Fees
  : address     * address * bytes  * string * tDestType * tTeznameInfo * tFees ) s =
  if (Current.sender () <> s.sAdmin) then Current.failwith "[ERROR]register/0";
  let cNametypes = castNameTypes s "[ERROR]register/1" in
  let op = cNametypes.nameCheck (_hname, _TeznameInfo) ~amount:0tz in
  let teznameType = info2type _TeznameInfo in
  let cRegistrar = castRegistrar s "[ERROR]register/2" in
  let param = (_parentAddr , _addr , _hname , _Dest , _DestType , teznameType , _Fees) in
  let amount = Current.amount () in
  let opRegister = cRegistrar.register param ~amount in
  ([op; opRegister] , s)

let%entry renew
  ( _parentAddr , _addr   , _TeznameType
  : address     * address * tTeznameType ) s =
  let cRenew = castRenew s "[ERROR]renew" in
  let amount = Current.amount () in
  let op = cRenew.renew (_parentAddr, _addr, _TeznameType) ~amount in
  ([op] , s)

let%entry update (_idx , _addr : tIdx * address) s =
  if (Current.sender () <> s.sAdmin) then Current.failwith "[ERROR]update";
  let s = begin match _idx with
    | Reg -> s.scRegistrar <- _addr
    | Rew -> s.scRenew     <- _addr
    | Nac -> s.scNameTypes <- _addr
    | Auc -> s.scAuction   <- _addr
    | RtN -> s.sRootName   <- _addr
  end in
  ([] : operation list) , s
