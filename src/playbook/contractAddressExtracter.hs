import Data.String
import System.Environment

main :: IO ()
main = do
  path <- fmap head $ getArgs
  ctx <- readFile path
  let x = (!! 2) $ head $ filter pred' $ map words $ lines ctx
  putStrLn x

pred' ("New":"contract":xs) = True
pred' _ = False
