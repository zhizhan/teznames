#!/bin/bash
#
if [ "$1" == "" ]; then
  basedir=$(git rev-parse --show-toplevel) || exit 1
else
  basedir="$1"
fi
echo "[info] set basedir=${basedir}"
#
echo "[info] loading ${basedir}/ENV.."
source "${basedir}/ENV" || exit 1
#
echo "[info] loading ${basedir}/ADDR.."
source "${basedir}/ADDR"  || exit 1
#
# ==========================
# ===== deploy tezname =====
# ==========================
#
depLv1Name () {
    echo -e "[info] calculating the blake2b for \033[33m${tezname}\033[39m"
    info=$(tezos-client -A "${alphanode}" hash data "\"${tezname}\"" of type string 2>&1 || exit 1)
    hname=$(stack runghc -- "${scriptdir}/getBlake2b.hs" "$info") || exit 1
    hname_short=$(echo "$hname" | cut -c3-10)
    #
    initS=""
    initS+="(Pair ${hname} "    # sTezname
    initS+="(Pair \"${admin}\" "   # sAdmin
    # --
    initS+="(Pair \"${admin}\" " # sDest
    initS+="(Pair \"${admin}\" " # sOwner
    # --
    initS+="(Pair (Left Unit) " # sTeznameType
    initS+="(Pair (Pair ${fee1}000000 (Pair ${fee2}000000 ${fee3}000000)) " # sFees
    initS+="(Pair {} "                        # sSubnameRecords
    # --
    initS+="(Pair \"2019-10-01T00:00:01Z\" "  # sRegistrationDate
    initS+="(Pair \"2020-10-01T00:00:00Z\" "  # sExpirationDate
    initS+="(Pair \"2019-10-01T00:00:00Z\" \"Created\"" # sLastModification
    initS+="))))))))))"
    #
    echo "[info] deploying tezname-${version}_${tezname}"
    #
    tezos-client -A "${alphanode}"\
      originate contract "tezname-${version}_${tezname}" \
      for "$admin" \
      transferring 0 from "$admin" \
      running "${contractdir}/cTezname.tz" \
      --init "$initS" \
      --burn-cap 16.0 \
      --force \
      > "${logdir}/tezname-${version}_${tezname}.deployment" 2>&1 || exit 1
    sleep 1
    #
    nameAddr=$(stack runghc -- "${scriptdir}/contractAddressExtracter.hs" "${logdir}/tezname-${version}_${tezname}.deployment") || exit 1
    echo -e "[info] \033[33m$tezname\033[39m(\033[32m${hname}\033[39m) has been originated at \033[32m$nameAddr\033[39m"
}
#
tezname="9chs"
fee1=1
fee2=1
fee3=1
depLv1Name
#
tezname="TezosFoundation"
fee1=10
fee2=50
fee3=120
depLv1Name
#
tezname="TezosSoutheastAsia"
fee1=10
fee2=15
fee3=30
depLv1Name
#
tezname="TezosJapan"
fee1=10
fee2=15
fee3=30
depLv1Name
#
tezname="TezosKorea"
fee1=10
fee2=15
fee3=30
depLv1Name
#
# ============================
# ===== register tezname =====
# ============================
#
# arg="(Left "
# arg+="(Pair \"${ROOTNAMEADDR}\" " # _parentAddr : address
# arg+="(Pair \"${nameAddr}\" " # _targetNameAddr : address
# arg+="(Pair ${hname} " # _hname : bytes
# arg+="(Left Unit))))" # _TeznameInfo : tTeznameInfo
# arg+=")"
# #
# # tezos-client -S -P 443 -A "${alphanode}" \
# tezos-client -A "${alphanode}" \
#   transfer 200 from "${admin}" \
#   to "${GATEADDR}" \
#   --arg "${arg}" \
#   --burn-cap 16.0 \
#   > "${logdir}/${nameAddr}.update" 2>&1 || exit 1
# echo -e "${tezname} registered"
# echo "ADDR01=${nameAddr}" >> "${basedir}/ADDR"
# #
